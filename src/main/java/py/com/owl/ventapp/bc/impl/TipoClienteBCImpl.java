package py.com.owl.ventapp.bc.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import py.com.owl.ventapp.base.BaseBCImpl;
import py.com.owl.ventapp.bc.TipoClienteBC;
import py.com.owl.ventapp.dao.TipoClienteDAO;
import py.com.owl.ventapp.domain.TipoCliente;
import py.una.cnc.htroot.core.dao.Dao;

@Component
@Scope("session")

public class TipoClienteBCImpl extends BaseBCImpl<TipoCliente> implements TipoClienteBC {

	@Autowired
	private TipoClienteDAO tipoClienteDao;

	@Override
	public Dao<TipoCliente> getDAOInstance() {
		// TODO Auto-generated method stub
		return tipoClienteDao;
	}

}
