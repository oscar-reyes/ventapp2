package py.com.owl.ventapp.bc.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.web.context.WebApplicationContext;
import py.com.owl.ventapp.base.BaseBC;
import py.com.owl.ventapp.bc.EmpresaBC;
import py.com.owl.ventapp.bc.SucursalBC;
import py.com.owl.ventapp.dao.EmpresaDao;
import py.com.owl.ventapp.domain.Empresa;
import py.com.owl.ventapp.domain.Sucursal;
import py.una.cnc.htroot.core.bc.impl.MasterDetailBCImpl;
import py.una.cnc.htroot.core.util.PersistResponse;

@Repository
@Scope(WebApplicationContext.SCOPE_SESSION)
public class EmpresaBCImpl extends MasterDetailBCImpl<Empresa, Sucursal> implements EmpresaBC {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	@Autowired
	private EmpresaDao empresaDao;
	@Autowired
	private SucursalBC sucursalBC;

	@Override
	public EmpresaDao getDAOInstance() {

		return empresaDao;
	}

	@Override
	public void beforeCreateOrEditDetail(Empresa empresa, Sucursal sucursal) {

		sucursal.setEmpresa(empresa);
	}

	@Override
	public BaseBC<Sucursal> getDetailBC() {

		return sucursalBC;
	}

	@Override
	public List<Sucursal> getMasterDetails(Empresa empresa) {

		return sucursalBC.getListByEmpresa(empresa);
	}

	@Override
	protected void setMasterPRList(Empresa empresa, List<PersistResponse<Sucursal>> perlist) {

		empresa.setSucursalPRList(perlist);

	}

	@Override
	protected void setMasterDetailList(Empresa empresa, List<Sucursal> details) {

		empresa.setSucursales(details);
	}

	@Override
	public List<Sucursal> getDetailsFromMaster(Empresa master) {

		return master.getSucursales();
	}

}
