package py.com.owl.ventapp.controllers.lists;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import py.com.owl.ventapp.base.BaseList;
import py.com.owl.ventapp.bc.TipoClienteBC;
import py.com.owl.ventapp.domain.TipoCliente;
import py.una.cnc.htroot.core.bc.BusinessController;

@Controller
@Scope("session")
@RequestMapping("/tipo-cliente")
public class TipoClienteList extends BaseList<TipoCliente> {
	@Autowired
	private TipoClienteBC tipoClienteBC;

	@Override
	public BusinessController<TipoCliente> getBusinessController() {
		// TODO Auto-generated method stub
		return tipoClienteBC;
	}

}
