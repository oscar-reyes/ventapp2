package py.com.owl.ventapp.dao.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import py.com.owl.ventapp.base.BaseDaoImpl;
import py.com.owl.ventapp.dao.TipoClienteDAO;
import py.com.owl.ventapp.domain.TipoCliente;

@Component
@Repository
@Scope("session")

public class TipoClienteDaoImpl extends BaseDaoImpl<TipoCliente> implements TipoClienteDAO {

}
