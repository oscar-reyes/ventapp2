package py.com.owl.ventapp.dao.impl;

import java.util.List;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
import py.com.owl.ventapp.base.BaseDaoImpl;
import py.com.owl.ventapp.dao.SucursalDao;
import py.com.owl.ventapp.domain.Empresa;
import py.com.owl.ventapp.domain.Sucursal;

@Repository
@Scope(WebApplicationContext.SCOPE_SESSION)
public class SucursalDaoImpl extends BaseDaoImpl<Sucursal> implements SucursalDao {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Transactional
	@Override
	public List<Sucursal> getListByEmpresa(Empresa empresa) {

		return findEntitiesByCondition("WHERE empresa_id = ?1", empresa.getId());
	}

}
