package py.com.owl.ventapp.dao.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.web.context.WebApplicationContext;
import py.com.owl.ventapp.base.BaseDaoImpl;
import py.com.owl.ventapp.dao.EmpresaDao;
import py.com.owl.ventapp.domain.Empresa;

@Repository
@Scope(WebApplicationContext.SCOPE_SESSION)
public class EmpresaDaoImpl extends BaseDaoImpl<Empresa> implements EmpresaDao {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

}
